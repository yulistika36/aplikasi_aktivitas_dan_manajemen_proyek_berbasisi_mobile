<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title>Mobilekit Mobile UI Kit</title>
    <meta name="description" content="Mobilekit HTML Mobile UI Kit">
    <meta name="keywords" content="bootstrap 4, mobile template, cordova, phonegap, mobile, html" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">
</head>

<body class="bg-white">

    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <!-- * loader -->

    <!-- App Header -->
    <div class="appHeader no-border transparent">
        <div class="left">
            <h3 align="left" style="padding-left: 29px; padding-top:20px;">Tambah Mata Kuliah Terkait
            </div>
            <a href="matkul.php" class="right" style="padding-right: 20px; padding-top:13px;"><ion-icon name="close"></ion-icon></a>
            </h3>
        </div>
    </div>
    </div>
    <!-- * App Header -->

    <!-- * App Capsule -->
    <div class="section mt-2">
        
            <div style="width: 100%; display: flex; justify-content: space-between;">
            </div>
            </div>
            <form action="simpan_matkul.php" method="post">
                <div style="padding-inline: 20px;" class="form-group boxed">
                    <div class="input-wrapper">
                        <div class="section mt-5">
                            <div style="width: 100%; height: 1px; margin-bottom:20px; background-color: #888888;">
                            </div>
                            <h4>Mata Kuliah</h4>
                            <input type="text" class="form-control" for="nama_matakuliah" id="nama_matakuliah" name="nama_matakuliah" style="background-color: #F5F5F5;">
                            <i class="clear-input">
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>
                </div>

                <div style="padding-inline: 20px;" class="form-group boxed">
                    <div class="input-wrapper">
                        <div class="section mt4">
                            <h4>Kode Mata Kuliah</h4>
                            <input type="text" class="form-control" id="kode_matakuliah" name="kode_matakuliah" style="background-color: #F5F5F5;">
                            <i class="clear-input">
                            </i>
                        </div>
                    </div>
                </div>

                <div style="padding-inline: 20px;" class="form-group boxed">
                    <div class="input-wrapper">
                        <div class="section mt4">
                            <h4>Dosen Koordinator</h4>
                            <input type="text" class="form-control"for="dosen_koor" id="dosen_koor" name="dosen_koor" style="background-color: #F5F5F5;">
                            <i class="clear-input">
                            </i>
                        </div>
                    </div>
                </div>

                <div style="padding-inline: 20px;" class="form-group boxed">
                    <div class="input-wrapper">
                        <div class="section mt4">
                            <h4>Capaian Mata Kuliah</h4>
                            <input type="text" class="form-control" for="capaian_matakuliah" id="capaian_matakuliah" name="capaian_matakuliah" style="background-color: #F5F5F5;">
                            <i class="clear-input">
                            </i>
                            <div class="form-button-group">
                                <button type="submit" class="btn btn-block btn-lg" style="background-color: #4543BD; color:white;">Tambahkan</button>
                            </div>
            </form>
             </div>
    </div>

<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function () {
        'use strict';
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>

    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="assets/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="assets/js/lib/popper.min.js"></script>
    <script src="assets/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.2.3/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>


</body>
</html>