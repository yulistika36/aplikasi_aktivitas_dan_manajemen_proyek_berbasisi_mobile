<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title>Siap PBL</title>
    <meta name="description" content="Mobilekit HTML Mobile UI Kit">
    <meta name="keywords" content="bootstrap 4, mobile template, cordova, phonegap, mobile, html" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">
</head>

<body>

    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <!-- * loader -->

    <!-- App Header -->
    <div class="appHeader text-light" style="background-color: #4543BD; color: #FFFFFF;">
        <div class="left">
            <a href="javascript:;" class="headerButton goBack">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">Identitas Tim</div>
        <div class="right"></div>
    </div>
    <!-- * App Header -->

    <!-- App Capsule -->
    <div id="appCapsule">

        <div class="section mt-2 mb-3">
            <div class="wide-block pt-2 pb-1">
                <p>
                    <div style="padding-inline: 5px;" class="form-group boxed">
                        <div class="input-wrapper">
                            <div style="display: flex; align-items: left; padding-left: 12px; padding-bottom: 10px;">
                                <ion-icon name="person-outline" style="height: 20px; width: 20px"></ion-icon>
                                <div class="section-title" style="padding-left:5px;">Identitas Tim</div>
                            </div>
                            <div style="width: 100%; height: 1px; margin-bottom:10px; background-color:#888888;">
                            </div>
                            <div class="section mt4">
                            </div>
                        </div>
                    </div>

                    <div style="padding-inline: 2px;" class="form-group boxed">
                        <div class="input-wrapper">
                            <div class="section mt4">
                                <h4>NIM Ketua</h4>
                                <input type="text" class="form-control" id="nim_ketua" placeholder="00000000000" style="background-color: #F5F5F5;">
                                <i class="clear-input">
                                </i>
                            </div>
                        </div>
                    </div>
                
                    <div style="padding-inline: 2px;" class="form-group boxed">
                        <div class="input-wrapper">
                            <div class="section mt4">
                                <h4>Nama Ketua</h4>
                                <input type="text" class="form-control" class="form-control" id="nama_ketua" placeholder="Nama Lengkap" style="background-color: #F5F5F5;">
                                <i class="clear-input">
                                </i>
                            </div>
                        </div>
                    </div>

                    <div style="padding-inline: 2px;" class="form-group boxed">
                        <div class="input-wrapper">
                            <div class="section mt4">
                                <h4>Nama Tim</h4>
                                <input type="text" class="form-control" id="nim_ketua" placeholder="Tim PBLIF-17" style="background-color: #F5F5F5;">
                                <i class="clear-input">
                                </i>
                            </div>
                        </div>
                    </div>
                
                    <div style="padding-inline: 2px;" class="form-group boxed">
                        <div class="input-wrapper">
                            <div class="section mt4">
                                <h4>Judul Tim</h4>
                                <input type="text" class="form-control" class="form-control" id="judul" placeholder="Aplikasi Aktivitas Manajemen Proyek" style="background-color: #F5F5F5;">
                                <i class="clear-input">
                                </i>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div style="text-align: right;" class="form-group boxed">
                            <div class="input-wrapper">
                                <div class="section mt4">
                                </div>
                                <div class="right" style= "padding-bottom: 250px;"></div>
                            </div>
                        <a href="#" class="btn" style="font-size: 15px; background-color: #FFA621; color: #FFFFFF;" >Ubah</a>
                        <a href="#" class="btn" style="font-size: 15px; background-color: #4543BD; color: #FFFFFF;" >Simpan</a>
                        </div>
                    </div>

                </p>
            </div>
        </div>
    </div>
</div>
        
    </div>

    
    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="assets/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="assets/js/lib/popper.min.js"></script>
    <script src="assets/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.2.3/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>


</body>

</html>