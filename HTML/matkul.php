<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title>Siap PBL</title>
    <meta name="description" content="Mobilekit HTML Mobile UI Kit">
    <meta name="keywords" content="bootstrap 4, mobile template, cordova, phonegap, mobile, html" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">

</head>

<body class="bg-white">
    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <!-- * loader -->

    <!-- App Header -->
    <div class="appHeader bg" style="background-color:#4543BD; color:#FFFFFF">
        <div class="left">
            <a href="index.php" class="headerButton goBack">
                <ion-icon name="chevron-back-outline" style="color: #FFFFFF;"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">Mata Kuliah</div>
        <div class="right"></div>
    </div>
    <!-- * App Header -->

    <div style="text-align: right;">
        <h4 class="subtitle" style="padding-top: 50px;"></h4>
        <a href="tambah-matakuliah.php" class="btn align-items-center" style="font-size: 15px; background-color: #FFA621; color: #FFFFFF; margin-right:15px; margin-top:10px;">Tambah</a>
    </div>
    <?php
    include 'koneksi.php';

    $query = mysqli_query($koneksi, "SELECT * FROM tb_daftar_matakuliah");
    while ($data = mysqli_fetch_assoc($query)) {
    ?>
        <div class="section mt-3 mb-3">
            <div class="card" style="background-color: #ffffff;">
                <div class="card-body">
                    <div class="agenda-item">
                        <h3 style="color: #4543BD;">Mata Kuliah Terkait :</h3>
                        <p><?php echo $data['kode_matakuliah']; ?> - <?php echo $data['nama_matakuliah']; ?></p>
                    </div>
                    <div style="width: 100%; height: 1px; margin-bottom:20px; background-color:#888888;">
                    </div>
                    <div class="agenda-item">
                        <p><?php echo $data['capaian_matakuliah']; ?></p>
                    </div>
                    <div class="agenda-item">
                        <h3 style="color: #4543BD;">Dosen Koordinator :</h3>
                        <p><?php echo $data['dosen_koor']; ?></p>
                    </div>
                    <div class="agenda-item">
                        <a type="button" class="btn btn-danger btn-block" onclick="konfirmasiHapus('<?php echo $data['id_matakuliah']?>')">Hapus</a>
                    </div>

                </div>
            </div>
        </div>
    <?php
    }
    ?>
<script>
        function konfirmasiHapus(id_matakuliah) {
        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Data akan dihapus permanen!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!"
        }).then((result) => {
            if (result.isConfirmed) {
                // Pindahkan panggilan fungsi hapus ke sini jika konfirmasi diaktifkan
                hapusData(id_matakuliah); 
            }
        });
    }

    // Fungsi untuk menghapus data (gantilah dengan fungsi penghapusan sesuai kebutuhan)
    function hapusData(id_matakuliah) {
        // Lakukan redirect ke halaman hapus_matkul.php dengan ID sebagai parameter
        window.location.href = 'hapus_matkul.php?id_matakuliah=' + id_matakuliah;
    }
</script>


    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="assets/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="assets/js/lib/popper.min.js"></script>
    <script src="assets/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.2.3/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>
    <script src="sweetalert2.min.js"></script>
    <link rel="stylesheet" href="sweetalert2.min.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

</body>

</html>