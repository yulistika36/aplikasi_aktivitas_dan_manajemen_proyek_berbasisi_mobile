<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title>Mobilekit Mobile UI Kit</title>
    <meta name="description" content="Mobilekit HTML Mobile UI Kit">
    <meta name="keywords" content="bootstrap 4, mobile template, cordova, phonegap, mobile, html" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">
</head>

<body class="bg-white">

    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <!-- * loader -->

    <!-- App Header -->
    <div class="appHeader no-border transparent">
        <div class="left">
            <h3 align="left" style="padding-left: 29px; padding-top:20px;">Tambah Logbook
            </div>
            <div class ="right" style="padding-right: 20px; padding-top:13px;" >
            <a href="index.php">
    <ion-icon name="close"></ion-icon>
</a>
            </h3>
        </div>
    </div>
    </div>
    <!-- * App Header -->

    <!-- * App Capsule -->
    <div class="section mt-2">
            <div style="width: 100%; display: flex; justify-content: space-between;">
            </div>
            </div>
    <form action="simpan_logbook.php" method="post">
    <div style="padding-inline: 20px;" class="form-group boxed">
        <div class="input-wrapper">
            <div class="section mt-5">
                <div style="width: 100%; height: 1px; margin-bottom:20px; background-color: #888888;">
                </div>
                <h4>Tahapan</h4>
                <input type="text" class="form-control" style="background-color: #F5F5F5;" class="form-control" id="tahapan" name="tahapan">
            </div>
        </div>
    </div>

    <div style="padding-inline: 20px;" class="form-group boxed">
        <div class="input-wrapper">
            <div class="section mt4">
                <h4>Deskripsi</h4>
                <textarea id="detail_pengerjaan"  name= "detail_pengerjaan" rows="4" class="form-control" style="background-color: #F5F5F5; "></textarea>
                <i class="clear-input">
                    <ion-icon name="close-circle"></ion-icon>
                </i>
            </div>
        </div>
    </div>

    <div style="padding-inline: 20px;" class="form-group boxed">
        <div class="input-wrapper">
            <div class="section mt4">
                <h4>Tanggal pengerjaan</h4>
                <input type="date" class="form-control" style="background-color: #F5F5F5;" id="tgl_mulai" name="tgl_mulai" >
            </div>
        </div>
    </div>

    <div style="padding-inline: 20px;" class="form-group boxed">
        <div class="input-wrapper">
            <div class="section mt4">
                <h4>Tanggal penyelesaian</h4>
                <input type="date" class="form-control" style="background-color: #F5F5F5;" id="tgl_selesai" name="tgl_selesai">
            </div>
        </div>
    </div>

    <div style="padding-inline: 20px;" class="form-group boxed">
        <div class="input-wrapper">
            <div class="section mt4">
                <h4>Output</h4>
                <input type="text" class="form-control" style="background-color: #F5F5F5;" id="output" name="output">
            </div>
        </div>
    </div>

    <div style="padding-inline: 20px;" class="form-group boxed">
        <div class="input-wrapper">
            <div class="section mt4">
                <h4>Kemajuan PBL</h4>
                <input type="number" class="form-control" id="persentase" name="persentase" required="" style="background-color: #F5F5F5;">
            <p class="help-block">Tuliskan kemajuan pengerjaan PBL dalam bentuk Persentase 1-100</p>
            </div>
        </div>
    </div>                
    <div class="form-button-group">
        <button id="tambahkanBtn" type="submit" class="btn btn-block btn-lg" style="background-color: #4543BD; color:white;">Tambahkan</button>
    </div>
            </form>

<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function () {
        'use strict';
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>

<script>
  // Function to show SweetAlert on button click
  function showSuccessAlert() {
    Swal.fire({
      title: "Logbook berhasil ditambahkan!",
      text: "",
      icon: "success",
      timer: 5000
    });
  }

  // Add click event listener to the button
  document.getElementById('tambahkanBtn').addEventListener('click', function() {
    // Call the function to show SweetAlert
    showSuccessAlert();
  });
</script>

    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="assets/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="assets/js/lib/popper.min.js"></script>
    <script src="assets/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.2.3/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>
    <script src="sweetalert2.min.js"></script>
    <link rel="stylesheet" href="sweetalert2.min.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

</body>
</html>
