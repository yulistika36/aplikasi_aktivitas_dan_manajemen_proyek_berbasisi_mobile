<?php

$id_usulan = isset($_POST['id_usulan']) ? $_POST['id_usulan'] : '';

if (!checkForeignKey($id_usulan)) {
    echo "Error: id_usulan tidak valid.";
} else {
}

function checkForeignKey($id_usulan) {
    include "koneksi.php";
    
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    // Periksa keberadaan id_usulan dalam tabel tb_usulan_judul
    $query = "SELECT id_usulan FROM tb_usulan_judul WHERE id_usulan = '$id_usulan'";
    $result = mysqli_query($conn, $query);

    // Tutup koneksi database
    mysqli_close($conn);

    // Kembalikan hasil
    return mysqli_num_rows($result) > 0;
}
?>

<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title>Mobilekit Mobile UI Kit</title>
    <meta name="description" content="Mobilekit HTML Mobile UI Kit">
    <meta name="keywords" content="bootstrap 4, mobile template, cordova, phonegap, mobile, html" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">
    <script src="https://kit.fontawesome.com/8a21c2c86c.js" crossorigin="anonymous"></script>

    <head>

        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <title>Berkasfile</title>
        </head>

    <body>
        <div class="appHeader bg" style="background-color: #4543BD; color: #ffffff;">
            <div class="left">
                <a href="index.php" class="headerButton">
                    <ion-icon name="chevron-back-outline" style="color: #ffffff;"></ion-icon>
                </a>
            </div>
            <div class="pageTitle">Berkasfile</div>
            <div class="right">

            </div>
        </div>

        <div class="container mt-5">
            <h1></h1>

            <div class="container mt-5" style="padding-top: 50px;">
                <div class="row justify-content-center">
                    <div class="col-md-12 text-center">
                        <h3>Berkas final UTS</h3>
                    </div>
                </div>


                <form action="simpan_uts.php" method="post" enctype="multipart/form-data">
                    <div class="row justify-content-center mt-4">
                        <div class="col-md-6">
                            <p>File Presentasi</p>
                            <input type="file" class="form-control" for="file_presentasi" id="file_presentasi" name="file_presentasi"><?php echo $data['file_presentasi']; ?>
                        </div>
                    </div>

                    <div class="row justify-content-center mt-4">
                        <div class="col-md-6">
                            <p>Link Video Presentasi</p>
                            <input type="text" class="form-control" for="link_youtube" id="link_youtube" name="link_youtube"><?php echo $data['link_youtube']; ?>
                            <!-- <button class="btn btn-primary mt-2">Unggah</button> -->
                        </div>
                    </div>

                    <div class="row justify-content-center mt-4">
                        <div class="col-md-6">
                            <p>Link Tambahan</p>
                            <input type="text" class="form-control" for="link_tambahan" id="link_tambahan" name="link_tambahan"><?php echo $data['link_tambahan']; ?>
                            <button class="btn btn-primary mt-2">Unggah</button>
                        </div>
                    </div>
                </form>

            </div>
            <div class="container mt-5">
                <div class="row justify-content-center">
                    <div class="col-md-12 text-center">
                        <h3>Berkas final UAS</h3>
                    </div>
                </div>
                <form action="simpan_uas.php" method="post" enctype="multipart/form-data">
                    <div class="row justify-content-center mt-4">
                        <div class="col-md-6">
                            <p>File Laporan Akhir</p>
                            <input type="file" class="form-control" for="file_laporan_akhir" id="file_laporan_akhir" name="file_laporan_akhir">
                            <!-- <button class="btn btn-primary mt-2">Unggah</button> -->
                        </div>
                    </div>

                    <div class="row justify-content-center mt-4">
                        <div class="col-md-6">
                            <p>File Presentasi</p>
                            <input type="file" class="form-control" for="file_presentasi" id="file_presentasi" name="file_presentasi">
                            <!-- <button class="btn btn-primary mt-2">Unggah</button> -->
                        </div>
                    </div>

                    <div class="row justify-content-center mt-4">
                        <div class="col-md-6">
                            <p>File Poster</p>
                            <input type="file" class="form-control" for="file_poster" id="file_poster" name="file_poster">
                            <!-- <button class="btn btn-primary mt-2">Unggah</button> -->
                        </div>
                    </div>

                    <div class="row justify-content-center mt-4">
                        <div class="col-md-6">
                            <p>File Dokumen HKI</p>
                            <input type="file" class="form-control" for="file_dokumen_hki" id="file_dokumen_hki" name="file_dokumen_hki">
                            <!-- <button class="btn btn-primary mt-2">Unggah</button> -->
                        </div>
                    </div>

                    <div class="row justify-content-center mt-4">
                        <div class="col-md-6">
                            <p>Link Youtube Presentasi</p>
                            <input type="file" class="form-control" for="link_youtube_presentasi" id="link_youtube_presentasi" name="link_youtube_presentasi">
                            <!-- <button class="btn btn-primary mt-2">Unggah</button> -->
                        </div>
                    </div>

                    <div class="row justify-content-center mt-4">
                        <div class="col-md-6">
                            <p>Link Youtube Demo</p>
                            <input type="file" class="form-control" for="link_youtube_demo" id="link_youtube_demo" name="link_youtube_demo">
                            <!-- <button class="btn btn-primary mt-2">Unggah</button> -->
                        </div>
                    </div>

                    <div class="row justify-content-center mt-4">
                        <div class="col-md-6">
                            <p>Link Manual Book</p>
                            <input type="file" class="form-control" for="link_manual_book" id="link_manual_book" name="link_manual_book">
                            <button class="btn btn-primary mt-2">Unggah</button>
                        </div>
                    </div>

                </form>
            </div>

            <div class="container mt-5">
                <div class="row justify-content-center">
                    <div class="col-md-12 text-center">
                        <h3>Berkas Pamer.in</h3>
                    </div>
                </div>
                <form action="simpan_pamerin.php" method="post" enctype="multipart/form-data">
                    <div class="row justify-content-center mt-4">
                        <div class="col-md-6">
                            <p>Gambar 1</p>
                            <input type="file" class="form-control" for="gambar1" name="gambar1" id="gambar1">
                            <!-- <button class="btn btn-primary mt-2">Unggah</button> -->
                        </div>
                    </div>

                    <div class="row justify-content-center mt-4">
                        <div class="col-md-6">
                            <p>Gambar 2</p>
                            <input type="file" class="form-control" for="gambar2" name="gambar2" id="gambar2">
                            <!-- <button class="btn btn-primary mt-2">Unggah</button> -->
                        </div>
                    </div>

                    <div class="row justify-content-center mt-4" style="padding-bottom: 100px;">
                        <div class="col-md-6">
                            <p>Gambar 3</p>
                            <input type="file" class="form-control" for="gambar3" name="gambar3" id="gambar3">
                            <button class="btn btn-primary mt-2">Unggah</button>
                        </div>
                    </div>
                </form>

            </div>


        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </body>

</html>




<!-- ///////////// Js Files ////////////////////  -->
<!-- Jquery -->
<script src="assets/js/lib/jquery-3.4.1.min.js"></script>
<!-- Bootstrap-->
<script src="assets/js/lib/popper.min.js"></script>
<script src="assets/js/lib/bootstrap.min.js"></script>
<!-- Ionicons -->
<script type="module" src="https://unpkg.com/ionicons@5.2.3/dist/ionicons/ionicons.js"></script>
<!-- Owl Carousel -->
<script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
<!-- jQuery Circle Progress -->
<script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
<!-- Base Js File -->
<script src="assets/js/base.js"></script>


</body>


</html>