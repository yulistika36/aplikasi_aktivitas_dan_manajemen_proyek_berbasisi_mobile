<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title>Siap PBL</title>
    <meta name="description" content="Mobilekit HTML Mobile UI Kit">
    <meta name="keywords" content="bootstrap 4, mobile template, cordova, phonegap, mobile, html" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">

</head>

<body class="bg-white">
    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <!-- * loader -->

    <!-- App Header -->
    <div class="appHeader bg" style="background-color:#4543BD; color:#FFFFFF">
        <div class="left">
            <a href="javascript:;" class="headerButton goBack">
                <ion-icon name="chevron-back-outline" style="color: #FFFFFF;"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">backlog</div>
        <div class="right"></div>
    </div>
    <!-- * App Header -->

    <div style="text-align: right;">
        <h4 class="subtitle" style="padding-top: 50px;"></h4>
        <a href="tambah-backlog.php" class="btn align-items-center" style="font-size: 15px; background-color: #FFA621; color: #FFFFFF; margin-right:15px; margin-top:10px;">Tambah</a>
    </div>

    <?php
    include 'koneksi.php';

    $query = mysqli_query($koneksi, "SELECT * FROM tb_kanban_board");
    while ($data = mysqli_fetch_assoc($query)) {
    ?>
        <div class="section mt-3 mb-2">
            <div class="card" style="background-color: #ffffff;">
                <div class="card-body">
                    <div class="agenda-item">
                        <div class="date-title">
                            <p align="right" class="date" style="font-size: x-small; border:#FFA621;"><?php echo $data['due_date']; ?></p>
                            <div class="tahapan-content">
                                <h3><?php echo $data['nim_mhs']; ?></h3>
                                <p><?php echo $data['judul']; ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="agenda-item">
                        <p><?php echo $data['isi_board']; ?></p>
                    </div>

                    <div class="agenda-item">
                    <a type="button" class="btn btn-danger btn-block" href="ubah_backlog.php?id_kanban_board=<?php echo $data['id_kanban_board']?>" style="color: white;">Ubah</a>
                    </div>
                </div>
            </div>
        </div>
    <?php
    }
    ?>

    <!-- App Bottom Menu -->
    <div class="appBottomMenu">
        <a href="index.html" class="item">
            <div class="col">
                <ion-icon name="home-outline"></ion-icon>
            </div>
        </a>
        <a href="app-components.html" class="item">
            <div class="col">
                <ion-icon name="cube-outline"></ion-icon>
            </div>
        </a>
        <a href="page-chat.html" class="item">
            <div class="col">
                <ion-icon name="chatbubble-ellipses-outline"></ion-icon>
                <span class="badge badge-danger">5</span>
            </div>
        </a>
        <a href="app-pages.html" class="item">
            <div class="col">
                <ion-icon name="layers-outline"></ion-icon>
            </div>
        </a>
        <a href="javascript:;" class="item" data-toggle="modal" data-target="#sidebarPanel">
            <div class="col">
                <ion-icon name="menu-outline"></ion-icon>
            </div>
        </a>
    </div>
    <!-- * App Bottom Menu -->

    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="assets/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="assets/js/lib/popper.min.js"></script>
    <script src="assets/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.2.3/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>

</body>

</html>