<?php
session_start();
$data = array(
    "username"        => $_POST['username'],
    "password"        => $_POST['password'],
    "token"           => $_POST['token'],
);

$ch = curl_init('http://sid.polibatam.ac.id/apilogin/web/api/auth/login');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result = json_decode(curl_exec($ch));

echo "Message   : " . $result->message . '<br>';

if ($result->status == "success") {
    $_SESSION['username'] = $result->data->name . '<br>';
    echo $_SESSION['username'];
    header("Location: index.php");

}

?>