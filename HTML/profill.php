<?php
session_start();
// Cek apakah session username sudah dibuat, jika tidak redirect ke halaman login
if (!isset($_SESSION['username'])) {
  header("Location: page-login.html");
  exit();
}
?>

<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title>Siap PBL</title>
    <meta name="description" content="Mobilekit HTML Mobile UI Kit">
    <meta name="keywords" content="bootstrap 4, mobile template, cordova, phonegap, mobile, html" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">
</head>

<body class="bg-white">

    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <!-- * loader -->

    <!-- App Header -->
    <div style="width: 100%; height: 70px; border-bottom-left-radius: 50%; border-bottom-right-radius: 50%; background-color: #4543BD;" class="bg">
    </div>
    <!-- * App Header -->
    
    <!-- App Capsule -->
    <div id="appCapsule">

        <div style="margin-top: -40px" class="login-form">
            <div class="section">
                <img src="assets/img/sample/photo/vector4.png" alt="avatar" class="imaged w64 rounded">
            </div>
            <div class="section mt-1">
                <h2><?php echo $_SESSION['username']; ?></h2>
            </div>
                </form>
            </div>
        </div>
    
    <!-- * App Capsule -->
    <div style="width: 100%; display: flex; justify-content: space-between; ">
        <div>
            <div style="width: 50px; height: 5px; margin-bottom: 20px; background-color: #4543BD" class="bg"></div>
            <div style="width: 125px; height: 5px; background-color: #4543BD" class="bg"></div>
        </div>
        <div style="display: flex; flex-direction: column; align-items: end;">
            <div style="width: 50px; height: 5px; margin-bottom: 20px; background-color: #4543BD" class="bg"></div>
            <div style="width: 125px; height: 5px; background-color: #4543BD" class="bg"></div>
        </div>
    </div>
    
    <div style="padding-inline: 20px;" class="form-group boxed">
            <div class="section mt-3">
                <h4>Nama</h4>
            </div>
            <div class="form-control" style="background-color: #F5F5F5; ">
        </div>
    </div>

    <div style="padding-inline: 20px;" class="form-group boxed">
            <div class="section mt4">
                <h4>Email</h4>
            </div>
            <div class="form-control" style="background-color: #F5F5F5;">
        </div>
    </div>

    <div style="padding-inline: 20px;" class="form-group boxed">
            <div class="section mt4">
                <h4>Email Polibatam</h4>
            </div>
            <div class="form-control" style="background-color: #F5F5F5;">
        </div>
    </div>

    <div style="padding-inline: 20px;" class="form-group boxed">
            <div class="section mt4">
                <h4>No Whatsapp</h4>
            </div>
            <div class="form-control" style="background-color: #F5F5F5;">
        </div>
    </div>

    <div class="section mt-1 text-center" style="padding-top: 50px;">
        <h2 align="center">Judul PBL</h2>
    </div>
    <table align="center" cellpadding="20" width="100%" >
        <tr>
            <td style="font-weight: bold;"> Judul Tim</td>
            <td>: Aplikasi Manajemen Proyek</td>
        </tr>
        <tr>
            <td style="font-weight: bold;">Manpro</td>
            <td>: Supardianto</td>
        </tr>
        <tr>
            <td style="font-weight: bold;">Nama Tim</td>
            <td>: PBLIF-17</td>
        </tr>
    </table>
    
    <div class="section mt-1 text-center" style="padding-top: 50px;">
        <h2 align="center">Tim PBL</h2>
    </div>
    <table align="center" cellpadding="20" width="100%" >
        <tr>
            <td style="font-weight: bold;"> 3312201071</td>
            <td>: Fatimah Azzahra (ketua)</td>
        </tr>
        <tr>
            <td style="font-weight: bold;">3312201045</td>
            <td>: Cindy Juliyanti (anggota)</td>
        </tr>
        <tr>
            <td style="font-weight: bold;">3312201054</td>
            <td>: Abdul Ghafur (anggota)</td>
        </tr>
        <tr>
            <td style="font-weight: bold;">3312201084</td>
            <td>: Rizqi Ammar (anggota)</td>
        </tr>
        <tr>
            <td style="font-weight: bold; padding-bottom:150px;">6042301008</td>
            <td style="padding-bottom:150px;">: Yulistika Wati (anggota)</td>
        </tr>
    </table>

    
    <div class="form-button-group" style=" padding-bottom:70px">
        <button type="submit" class="btn btn-block btn-lg" style="background-color: #4543BD; color:white;" onclick="logout()" >
        <a href="logout.php">Keluar</a>
    </button>
    </div>

 <!-- App Bottom Menu -->
 <div class="appBottomMenu">
    <a href="index.php" class="item">
        <div class="col">
            <ion-icon name="home-outline"></ion-icon>
        </div>
    </a>
    <a href="page-chat.php" class="item">
        <div class="col">
            <ion-icon name="chatbubble-ellipses-outline"></ion-icon>
            <span class="badge badge-danger">5</span>
        </div>
    </a>
    <a href="agenda.php" class="item">
        <div class="col">
                <ion-icon name="calendar-outline"></ion-icon>
        </div>
    </a>
    <a href="profill.php" class="item">
        <div class="col">
            <ion-icon name="person-outline" role="img" class="md hydrated" aria-label="person out line"></ion-icon>
        </div>
    </a>
<!-- * App Bottom Menu -->

    

    
    
    



<!-- ///////////// Js Files ////////////////////  -->
<!-- Jquery -->
<script src="assets/js/lib/jquery-3.4.1.min.js"></script>
<!-- Bootstrap-->
<script src="assets/js/lib/popper.min.js"></script>
<script src="assets/js/lib/bootstrap.min.js"></script>
<!-- Ionicons -->
<script type="module" src="https://unpkg.com/ionicons@5.2.3/dist/ionicons/ionicons.js"></script>
<!-- Owl Carousel -->
<script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
<!-- jQuery Circle Progress -->
<script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
<!-- Base Js File -->
<script src="assets/js/base.js"></script>


</body>

</html>