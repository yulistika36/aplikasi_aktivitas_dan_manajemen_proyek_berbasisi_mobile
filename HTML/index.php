<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title>Siap PBL</title>
    <meta name="description" content="Mobilekit HTML Mobile UI Kit">
    <meta name="keywords" content="bootstrap 4, mobile template, cordova, phonegap, mobile, html" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">
    <script src="https://kit.fontawesome.com/8a21c2c86c.js" crossorigin="anonymous"></script>

</head>

<body>


    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <!-- * loader -->

    <div class="header-large-title">
                    <h3 class="title">SIAP-PBL<ion-icon name="chevron-down">
                    </ion-icon></h3> 
                    <h4 class="subtitle">Selamat Datang di SIAP-PBL</h4>
                </div>
            
        <div class="section mt-3 mb-3">
            <div class="card" style="background-color: #4543BD;">
                <div class="card-body d-flex justify-content-between align-items-center">
                    <h6 class="card-title d-flex align-items-center" style="color: white; font-size: large;">
                        <img src="assets/img/kanban.png" alt="image" class="imaged w-24 mr-1 ml-1" >
                        Papan Kanban
                    </h6>
                    <a href="backlog.php" class="btn" style="font-size: 15px; background-color: #ffd503; color: white; font-size:medium" >Lihat</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="section mt-3 mb-3">
            <dAiv class="card" style="background-color: #4543BD;" >
                <div class="card-body d-flex justify-content-between align-items-end">
                    <h6 class="card-title d-flex align-items-center" style="color: white; font-size: large;">
                        <img src="assets/img/logbook.png" alt="image" class="imaged w-24 mr-2 ml-1"->
                        Logbook</h6>
                    <a href="logbook.php" class="btn" style="font-size: 15px; background-color: #ffd503; color: white; font-size:medium">Lihat</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="section mt-3 mb-3">
            <div class="card" style="background-color: #4543BD;" >
                <div class="card-body d-flex justify-content-between align-items-end">
                    <h6 class="card-title d-flex align-items-center" style="color: white; font-size: large;">
                        <img src="assets/img/berkas.png" alt="image" class="imaged w-24 mr-1 ml-1"->
                        Upload berkas
                    </h6>
                    <a href="berkas-file.php" class="btn" style="font-size: 15px; background-color: #ffd503; color: white; font-size:medium">Lihat</a>
                    </div>
                </div>
                <h3 class="subtitle" style="padding-top: 20px; font-weight: bold;">Menu Ketua Tim</h3>
                <div class="section mt-3 mb-3">
                        <div class="carousel-multiple owl-carousel owl-theme">
                            <a href= "identitas.php">
                            <div class="item">
                                <div class="card">
                                    <div class="card-body pt-2" style="background-color: #CAFFB8; border-radius: 10px;">
                                        <h4 class="mb-0" style="color: rgb(0, 0, 0);" align="center">Kelola Identitas Tim</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href= "anggota.php">
                            <div class="item">
                                <div class="card">
                                    <div class="card-body pt-2" style="background-color: #FFD9D9; border-radius: 10px;">
                                        <h4 class="mb-0" style="color: rgb(0, 0, 0);" align="center">Kelola Anggota Tim</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href= "matkul.php">
                            <div class="item">
                                <div class="card">
                                    <div class="card-body pt-2" style="background-color: #B7E9FF; border-radius: 10px;">
                                        <h4 class="mb-0" style="color: rgb(0, 0, 0);" align="center">Kelola Mata Kuliah</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                        </div>
                    </div>
                    <h3 class="subtitle" style="font-weight: bold;">Agenda terbaru</h3>
                    <p>Berikan komentar pada agenda terbaru!</p>
                </div>
                <div class="section mt-3 mb-3">
                    <div class="card" style="background-color: #ffffff;">
                        <div class="card-body d-flex justify-content-between align-items-center">
                            <div class="agenda-item">
                                <h3>Judul Agenda</h3>
                                <p>Deskripsi Agenda: Ini adalah deskripsi singkat tentang agenda ini.</p>
                            </div>
                            </div>
                            <p align="right" style="padding-right:25px;">10.00-14.30, 30 Oktober 2023</p>
                            <div class="agenda-item" style="padding-bottom: 15px; text-align: center;">
                                <a href="agenda.php" class="btn" style="background-color: #4543BD; color:#F5F5F5; font-size:medium">Lihat</a>
                            </div>
                        </div>
                    </div>


            

        <!-- App Bottom Menu -->
     <div class="appBottomMenu">
        <a href="index.php" class="item">
            <div class="col">
                <ion-icon name="home-outline"></ion-icon>
            </div>
        </a>
        <a href="page-chat.php" class="item">
            <div class="col">
                <ion-icon name="chatbubble-ellipses-outline"></ion-icon>
                <span class="badge badge-danger">5</span>
            </div>
        </a>
        <a href="agenda.php" class="item">
            <div class="col">
                    <ion-icon name="calendar-outline"></ion-icon>
            </div>
        </a>
        <a href="profill.php" class="item">
            <div class="col">
                <ion-icon name="person-outline" role="img" class="md hydrated" aria-label="person out line"></ion-icon>
            </div>
        </a>
    <!-- * App Bottom Menu -->

        

    <script>
        function redirectToSelectedProject() {
            // Ambil nilai proyek yang dipilih
            var selectedProject = document.getElementById("projectSelect").value;

            // Redirect ke halaman proyek yang dipilih
            // Sesuaikan URL pengalihan dengan kebutuhan Anda
            window.location.href = "logbook.php?id=" + selectedProject;
        }
    </script>
        
        

    

    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="assets/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="assets/js/lib/popper.min.js"></script>
    <script src="assets/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.2.3/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>


</body>

</html>