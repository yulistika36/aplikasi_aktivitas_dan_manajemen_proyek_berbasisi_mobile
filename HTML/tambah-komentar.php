<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Komentar</title>
    <link rel="stylesheet" href="assets/css/style.css">
    <style>
        .profile-icon {
            display: flex;
            align-items: center;
        }

        .profile-icon ion-icon {
            font-size: 3rem; /* Adjust the size as needed */
            margin-right: 10px; /* Adjust the spacing as needed */
        }

        .comment-text {
            flex: 1;
        }
    </style>
</head>

<body class="bg-white">


    <!-- App Header -->
    <div class="appHeader bg" style="background-color: #4543BD; color: #ffffff;">
        <div class="left">
            <a href="index.html" class="headerButton">
                <ion-icon name="chevron-back-outline" style="color: #ffffff;"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">Komentar</div>
        <div class="right">
           
        </div>
    </div>
    <!-- * App Header -->
    <div class="container mt-5" style="padding-top: 30px;" style="padding-bottom: 40px;">

    <div class="section mt-3 mb-3">
        <div class="card" style="background-color: #ffffff;">
            <div class="card-body">
                <div class="comment-item">
                <div class="profile-icon">
                <ion-icon name="person-circle-outline"></ion-icon>
                        <p><strong>Aurel</strong> Pertemuannya dimana ya?</p>
                    </div>
                    <div class="comment-text">
                        <p>3312201071</p>
                        <p>3 hari</p>
                    </div>
                </div>
                <div class="comment-item">
                    <div class="profile-icon">
                        <ion-icon name="person-circle-outline"></ion-icon>
                        <p><strong>Bagas Saputra</strong> Saya izin tidak hadir</p>

                    </div>
                    <div class="comment-text">
                        <p>3312201071</p>
                        <p>2 hari</p>
                    </div>
                </div>
                <div class="comment-item">
                    <div class="profile-icon">
                        <ion-icon name="person-circle-outline"></ion-icon>
                        <p><strong>Saga Aksara</strong> Apakah diperbolehkan tidak hadir?</p>
                    </div>
                    <div class="comment-text">
                        <p>3312201071</p>
                        <p>2 hari</p>
                    </div>
                </div>
                <div class="comment-item">
                    <div class="profile-icon">
                        <ion-icon name="person-circle-outline"></ion-icon>
                        <p><strong>Anastashia</strong> Terima kasih atas informasinya.</p>
                    </div>
                    <div class="comment-text">
                        <p>3312201071</p>
                        <p>1 hari</p>
                    </div>
                    <div class="form-group" style="padding-top: 15px">
    <label style="display: block;">Isi komentar 
        <div style="display: flex; align-items: center;">
            <textarea name="isi" class="form-control" rows="1"></textarea>
            <button type="button" class="btn btn-icon btn-transparant rounded" style="margin-left: 10px;">
                <ion-icon name="send" style="color: #4543BD;"></ion-icon>
            </button>
        </div>
    </label>
</div>

                </div>
            </div>
        </div>
    </div>



   
    <!-- Js Files -->
    <script src="assets/js/lib/jquery-3.4.1.min.js"></script>
    <script src="assets/js/lib/popper.min.js"></script>
    <script src="assets/js/lib/bootstrap.min.js"></script>
    <script type="module" src="https://unpkg.com/ionicons@5.2.3/dist/ionicons/ionicons.js"></script>
    <script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <script src="assets/js/base.js"></script>
</body>

</html>
