<?php
include 'koneksi.php';
$id_kanban_board = $_GET['id_kanban_board'];
$result = mysqli_query($koneksi, "SELECT * FROM tb_kanban_board WHERE id_kanban_board='$id_kanban_board'");
while ($user_data = mysqli_fetch_array($result)) {
    $judul = $user_data['judul'];
    $isi_board = $user_data['isi_board'];
    $due_date = $user_data['due_date'];
    $mahasiswa = $user_data['mahasiswa'];
}
?>
<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title>Siap PBL</title>
    <meta name="description" content="Mobilekit HTML Mobile UI Kit">
    <meta name="keywords" content="bootstrap 4, mobile template, cordova, phonegap, mobile, html" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">
</head>

<body>

    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <!-- * loader -->

    <!-- App Header -->
    <div class="appHeader bg-primary text-light">
        <div class="left">
            <a href="javascript:;" class="headerButton goBack">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">Tambah Backlog</div>
        <div class="right"></div>
    </div>
    <!-- * App Header -->

    <!-- App Capsule -->
    <div id="appCapsule">

        <div class="section full mt-2 mb-2">
            <div class="section-title">Tambah backlog baru</div>
            <div class="wide-block pt-2 pb-2">
                <form action="update_backlog.php" method="post">
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <label class="label" for="judul">Judul</label>
                            <input type="text" class="form-control" id="judul" name="judul" value=<?php echo $judul; ?>>
                            <i class="clear-input">
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>

                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <label class="label" for="isi_board">Deskripsi</label>
                            <textarea id="isi_board" rows="4" class="form-control" name="isi_board" value=<?php echo $isi_board; ?>></textarea>
                            <i class="clear-input">
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>

                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <label class="label" for="due_date">Tenggat</label>
                            <input type="date" class="form-control" id="due_date" name="due_date" value=<?php echo $due_date; ?>>
                            <i class="clear-input">
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>


                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <label class="label" for="mahasiswa">Pilih Mahasiswa</label>
                            <select class="form-control custom-select" id="mahasiswa" name="mahasiswa" value=<?php echo $mahasiswa; ?>>
                                <option value="0">Pilih mahasiswa</option>
                                <option value="1">Cindy</option>
                                <option value="2">Fatimah</option>
                                <option value="3">Ghafur</option>
                                <option value="4">Yulistika</option>
                                <option value="5">Afif</option>
                            </select>
                        </div>
                    </div>

                    <div class="mt-2">
                        <button class="btn btn-primary btn-block" type="submit">Update</button>
                    </div>


                </form>

            </div>
        </div>


    </div>
    </div>






    <!-- * App Capsule -->

    <!-- App Bottom Menu -->
    <div class="appBottomMenu">
        <a href="index.html" class="item">
            <div class="col">
                <ion-icon name="home-outline"></ion-icon>
            </div>
        </a>
        <a href="page-chat.html" class="item">
            <div class="col">
                <ion-icon name="chatbubble-ellipses-outline"></ion-icon>
                <span class="badge badge-danger">5</span>
            </div>
        </a>
        <a href="app-pages.html" class="item">
            <div class="col">
                <ion-icon name="calendar-outline"></ion-icon>
            </div>
        </a>
        <a href="app-pages.html" class="item">
            <div class="col">
                <ion-icon name="person-outline" role="img" class="md hydrated" aria-label="person out line"></ion-icon>
            </div>
        </a>
        <!-- * App Bottom Menu -->

        <!-- * App Sidebar -->

        <!-- ///////////// Js Files ////////////////////  -->
        <!-- Jquery -->
        <script src="assets/js/lib/jquery-3.4.1.min.js"></script>
        <!-- Bootstrap-->
        <script src="assets/js/lib/popper.min.js"></script>
        <script src="assets/js/lib/bootstrap.min.js"></script>
        <!-- Ionicons -->
        <script type="module" src="https://unpkg.com/ionicons@5.2.3/dist/ionicons/ionicons.js"></script>
        <!-- Owl Carousel -->
        <script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
        <!-- jQuery Circle Progress -->
        <script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
        <!-- Base Js File -->
        <script src="assets/js/base.js"></script>


        <script>
            // Example starter JavaScript for disabling form submissions if there are invalid fields
            (function() {
                'use strict';
                window.addEventListener('load', function() {
                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                    var forms = document.getElementsByClassName('needs-validation');
                    // Loop over them and prevent submission
                    var validation = Array.prototype.filter.call(forms, function(form) {
                        form.addEventListener('submit', function(event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();
        </script>


</body>

</html>