<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title>Siap PBL</title>
    <meta name="description" content="Mobilekit HTML Mobile UI Kit">
    <meta name="keywords" content="bootstrap 4, mobile template, cordova, phonegap, mobile, html" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">
</head>

<body class="bg-white">

    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <!-- * loader -->

    <!-- App Header -->
    <div class="appHeader no-border transparent">
        <div class="left">
            <h3 align="left" style="padding-left: 29px; padding-top:20px;">Tambahkan Anggota Baru</h3>
        </div>
        <a href="anggota.php" class="right" style="padding-right: 20px; padding-top:13px;"><ion-icon name="close"></ion-icon></a>
    </div>
    <!-- * App Header -->

    <!-- * App Capsule -->
    <div class="section mt-2">
        <div style="width: 100%; display: flex; justify-content: space-between;">
        </div>
    </div>

    <form action="simpan_anggota.php" method="post" class="needs-validation" novalidate>

        <div style="padding-inline: 20px;" class="form-group boxed">
            <div class="input-wrapper">
                <div class="section mt-5">
                    <div style="width: 100%; height: 1px; margin-bottom:20px; background-color: #888888;"></div>
                    <h4>NIM</h4>
                    <input type="int" class="form-control" for="nim_mhs" id="nim_mhs" name="nim_mhs"
                        style="background-color: #F5F5F5;">
                    <i class="clear-input">
                        <ion-icon name="close-circle"></ion-icon>
                    </i>
                </div>
            </div>
        </div>

        <div style="padding-inline: 20px;" class="form-group boxed">
            <div class="input-wrapper">
                <div class="section mt4">
                    <h4>Nama</h4>
                    <input type="text" class="form-control" id="nama" name="nama"
                        style="background-color: #F5F5F5;">
                    <i class="clear-input"></i>
                </div>
            </div>
        </div>

        <!-- <div style="padding-inline: 20px;" class="form-group boxed">
            <div class="input-wrapper">
                <div class="section mt4">
                    <h4>Email</h4>
                    <input type="email" class="form-control" id="email_lain" name="email_lain"
                        style="background-color: #F5F5F5;">
                    <i class="clear-input"></i>
                </div>
            </div>
        </div> -->

        <div style="padding-inline: 20px;" class="form-group boxed">
            <div class="input-wrapper">
                <div class="section mt4">
                    <h4 align="left">Pilih peran</h4>
                    <select class="custom-select" id="peran" name="peran" required style="background-color: #F5F5F5;">
                        <option value="0">Pilih peran</option>
                        <option value="Ketua">ketua</option>
                        <option value="Anggota">anggota</option>
                    </select>
                    <div class="form-button-group">
                        <button type="submit" class="btn btn-block btn-lg" style="background-color: #4543BD; color:white;">Tambahkan</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    

    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="assets/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="assets/js/lib/popper.min.js"></script>
    <script src="assets/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.2.3/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>

</body>

</html>
