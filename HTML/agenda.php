<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title>Mobilekit Mobile UI Kit</title>
    <meta name="description" content="Mobilekit HTML Mobile UI Kit">
    <meta name="keywords" content="bootstrap 4, mobile template, cordova, phonegap, mobile, html" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">
    <script src="https://kit.fontawesome.com/8a21c2c86c.js" crossorigin="anonymous"></script>

</head>

<body class="bg-white">
     <!-- loader -->
     <div id="loader">
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <!-- * loader -->

    <!-- App Header -->
    <!-- App Header -->
    <div class="appHeader bg" style="background-color: #4543BD; color: #ffffff;">
        <div class="pageTitle">Jadwal Agenda Penting</div>
        <div class="right">

        </div>
    </div>
    <!-- * App Header -->
    <!-- * App Header -->

    <!-- App Capsule -->
     <div>
        <h4 class="subtitle" style="padding-bottom: 50px;"></h4>
    </div>

    <div class="section mt-3 mb-3">
        <div class="card" style="background-color: #ffffff;">
            <div class="card-body d-flex justify-content-between align-items-center">
                <div class="agenda-item">
                    <h3>Judul Agenda</h3>
                    <p>Deskripsi Agenda: Ini adalah deskripsi singkat tentang agenda ini.</p>
                </div>
                <a href="page-chat.html" class="item">
                    <div class="col" style="font-size: xx-large;">
                        <ion-icon name="chatbubble" style="color: #4543BD;"></ion-icon>
                    </div>
                </a>
            </div>
            <p align="right" style="padding-right:25px;">10.00-14.30, 30 Oktober 2023</p>
        </div>
    </div>

        <div class="section mt-3 mb-3">
            <div class="card" style="background-color: #ffffff;">
                <div class="card-body d-flex justify-content-between align-items-center">
                    <div class="agenda-item">
                        <h3>Judul Agenda</h3>
                        <p>Deskripsi Agenda: Ini adalah deskripsi singkat tentang agenda ini.</p>
                    </div>
                    <a href="page-chat.html" class="item">
                        <div class="col" style="font-size: xx-large;">
                            <ion-icon name="chatbubble" style="color: #4543BD;"></ion-icon>
                        </div>
                    </a>
                    </div>
                    <p align="right" style="padding-right:25px;">10.00-14.30, 30 Oktober 2023</p>
                </div>
            </div>

        <div class="section mt-3 mb-3">
            <div class="card" style="background-color: #ffffff;">
                <div class="card-body d-flex justify-content-between align-items-center">
                    <div class="agenda-item">
                        <i class="ion-chatbox-working"></i>
                        <h3>Judul Agenda</h3>
                        <p>Deskripsi Agenda: Ini adalah deskripsi singkat tentang agenda ini.</p>
                    </div>
                    <a href="page-chat.html" class="item">
                        <div class="col" style="font-size: xx-large;">
                            <ion-icon name="chatbubble" style="color: #4543BD;"></ion-icon>
                        </div>
                    </a>
                    </div>
                    <p align="right" style="padding-right:25px;">10.00-14.30, 30 Oktober 2023</p>
                </div>
            </div>

        <div class="section mt-3 mb-3">
            <div class="card" style="background-color: #ffffff;">
                <div class="card-body d-flex justify-content-between align-items-center">
                    <div class="agenda-item">
                        <h3>Judul Agenda </h3>
                        <p>Deskripsi Agenda: Ini adalah deskripsi singkat tentang agenda ini.</p>
                    </div>
                    <a href="page-chat.html" class="item">
                        <div class="col" style="font-size: xx-large;">
                            <ion-icon name="chatbubble" style="color: #4543BD;"></ion-icon>
                        </div>
                    </a>
                    </div>
                    <p align="right" style="padding-right:25px;">10.00-14.30, 30 Oktober 2023</p>
                </div>
            </div>

        <div class="section mt-3 mb-3" style="padding-bottom: 30px;">
            <div class="card" style="background-color: #ffffff;">
                <div class="card-body d-flex justify-content-between align-items-center">
                    <div class="agenda-item">
                        <i class="ion-chatbox-working"></i>
                        <h3>Judul Agenda</h3>
                        <p>Deskripsi Agenda: Ini adalah deskripsi singkat tentang agenda ini.</p>
                    </div>
                    <a href="page-chat.html" class="item">
                        <div class="col" style="font-size: xx-large;">
                            <ion-icon name="chatbubble" style="color: #4543BD;"></ion-icon>
                        </div>
                    </a>
                    </div>
                    <p align="right" style="padding-right:25px;">10.00-14.30, 30 Oktober 2023</p>
                </div>
            </div>


 <!-- App Bottom Menu -->
 <div class="appBottomMenu">
    <a href="index.php" class="item">
        <div class="col">
            <ion-icon name="home-outline"></ion-icon>
        </div>
    </a>
    <a href="page-chat.php" class="item">
        <div class="col">
            <ion-icon name="chatbubble-ellipses-outline"></ion-icon>
            <span class="badge badge-danger">5</span>
        </div>
    </a>
    <a href="agenda.php" class="item">
        <div class="col">
                <ion-icon name="calendar-outline"></ion-icon>
        </div>
    </a>
    <a href="profill.php" class="item">
        <div class="col">
            <ion-icon name="person-outline" role="img" class="md hydrated" aria-label="person out line"></ion-icon>
        </div>
    </a>
<!-- * App Bottom Menu -->

    

    
    
    


    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="assets/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="assets/js/lib/popper.min.js"></script>
    <script src="assets/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.2.3/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>



</body>

</html>
