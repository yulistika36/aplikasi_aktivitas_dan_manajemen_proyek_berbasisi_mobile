<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title>Siap PBL</title>
    <meta name="description" content="Mobilekit HTML Mobile UI Kit">
    <meta name="keywords" content="bootstrap 4, mobile template, cordova, phonegap, mobile, html" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">
</head>

<body class="bg-white">
    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <!-- * loader -->

    <!-- App Header -->
    <div class="appHeader bg" style="background-color: #4543BD; color: #ffffff;">
        <div class="left">
            <a href="index.php" class="headerButton">
                <ion-icon name="chevron-back-outline" style="color: #ffffff;"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">Kelola Anggota Tim</div>
        <div class="right">

        </div>
    </div>
    <!-- * App Header -->

    <!-- App Capsule -->
    <div style="text-align: right;">
        <div style="text-align: right;">
        <h4 class="subtitle" style="padding-top: 50px;"></h4>
        <a href="tambah-anggota.php" class="btn align-items-center" style="font-size: 15px; background-color: #FFA621; color: #FFFFFF; margin-right:15px; margin-top:10px;">Tambah</a>
    </div>
    </div>
<?php
include 'koneksi.php';

$query = mysqli_query($koneksi, "SELECT * FROM tb_tim_pbl_mhs");
while ($data = mysqli_fetch_assoc($query)) {
?>
    <div class="section mt-3 mb-3">
        <div class="card" style="background-color: #ffffff;">
            <div class="card-body d-flex justify-content-between align-items-center">
                <div class="agenda-item">
                    <i class="ion-chatbox-working"></i>
                    <div class="agenda-item" style="padding-bottom: 15px; text-align: center; background-color: #4543BD; color: white; border-radius: 5px;"><?php echo $data['peran']; ?></div>
                    <h3 style="padding-top: 20px;"><?php echo $data['nama_mhs']; ?></h3>
                    <p><?php echo $data['nim_mhs']; ?></p>
                    <a type="button" class="btn btn-danger btn-block" href="hapus-anggota.php?id_tim=<?php echo $data['id_tim']?>" style="color: white;">Hapus</a>
                </div>
            </div>
        </div>
    </div>
<?php
}
?>
    


    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="assets/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="assets/js/lib/popper.min.js"></script>
    <script src="assets/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.2.3/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>


</body>

</html>