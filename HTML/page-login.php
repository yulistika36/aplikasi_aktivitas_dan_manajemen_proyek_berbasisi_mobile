<?php
session_start();

// Check if the form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $data = array(
        "username" => $_POST['username'],
        "password" => $_POST['password'],
        "token"    => $_POST['token'],
    );

    $ch = curl_init('http://sid.polibatam.ac.id/apilogin/web/api/auth/login');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = json_decode(curl_exec($ch));

    if (!empty($errorMessage)) {
       
    }

    if ($result->status == "success") {
        $_SESSION['username'] = $result->data->name;
        header("Location: index.php");
        exit();
    }
}
?>

<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title>Aplikasi SIAP-PBL</title>
    <meta name="description" content="Mobilekit HTML Mobile UI Kit">
    <meta name="keywords" content="bootstrap 4, mobile template, cordova, phonegap, mobile, html" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="manifest" href="__manifest.json">
</head>


    <body class="bg-white">

        <!-- loader -->
        <div id="loader">
            <div class="spinner-border text-primary" role="status"></div>
        </div>
        <!-- * loader -->
    
    
        <!-- App Capsule -->
        <div id="appCapsule" class="pt-0">
    
            <div class="login-form mt-4">
                <div class="section">
                <img src="assets/img/login.png" alt="image" class="form-image"  >
                </div>
                <div class="section mt-1">
                    <h1 style="padding-bottom: 10px;">SIAP PBL</h1>
                    <h4 style="padding-bottom: 25px;">Sistem Informasi Aktivitas Proyek PBL</h4>
                </div>
    
    
                <div class="section mt-1 mb-5">
                    <form action="page-login.php" method="post">
                        <div class="form-group has-feedback">
                            <select class = "custom-select" name="Status" id="status" required="required">
                                <option value> Pilih jenis pengguna</option>
                                    <option value="Internal">Internal (Dokpol atau Learning)</option>
                                <option> Eksternal</option>
                            </select> 
                        </div>
                    
    
                        <div class="form-group has-feedback">
                            <div class="input-wrapper">
                                <input type="email" class="form-control" id="username" name="username"
                                placeholder="Email address">
                                <i class="clear-input mr-2">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>
                        
                        
                        <div class="form-group has-feedback">
                            <div class="input-wrapper">
                                <input type="password" class="form-control" id="password" name="password"
                                placeholder="Password">
                                <i class="clear-input mr-2">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <div class="input-wrapper">
                                <input type="password" class="form-control" id="token" name="token" placeholder="token">
                                <i class="clear-input mr-2">
                                    <ion-icon name="close-circle"></ion-icon>
                                </i>
                            </div>
                        </div>
    
                        <div class="form-button-group">
                            <button type="submit" class="btn btn-block btn-lg" style="background-color: #4543BD; color:white;" name="login" value="submit" value="login" >Masuk</button>
                        </div>
        
                    </form>
                </div>
            </div>
            </div>


            <script>
    // Check if there is a message to display
    const message = "<?php echo isset($result->message) ? $result->message : ''; ?>";

    if (message) {
        const errorContainer = document.createElement('div');
        errorContainer.className = 'alert alert-danger';
        errorContainer.textContent = message;

        const tokenInput = document.getElementById('token');
        const parentDiv = tokenInput.parentNode;

        // Create a margin container to add space
        const marginContainer = document.createElement('div');
        marginContainer.style.marginTop = '15px'; // Set the margin as needed
        parentDiv.insertBefore(marginContainer, tokenInput.nextSibling);

        // Insert the error message after the margin container
        marginContainer.appendChild(errorContainer);
    }
</script>

    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="assets/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="assets/js/lib/popper.min.js"></script>
    <script src="assets/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.2.3/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="assets/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>


</body>

</html>