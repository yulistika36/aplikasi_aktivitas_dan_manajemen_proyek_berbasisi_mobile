-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 23, 2023 at 04:25 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_siappbl`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_berkas_pamerin`
--

CREATE TABLE `tb_berkas_pamerin` (
  `id_berkas_pamerin` int(11) NOT NULL,
  `id_usulan` int(11) NOT NULL,
  `id_tim` int(11) NOT NULL,
  `nim_mhs` varchar(20) NOT NULL,
  `gambar1` text NOT NULL,
  `gambar2` text NOT NULL,
  `gambar3` text NOT NULL,
  `gambar4` text NOT NULL,
  `gambar5` text NOT NULL,
  `tgl_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_berkas_uas`
--

CREATE TABLE `tb_berkas_uas` (
  `id_berkas_uas` int(11) NOT NULL,
  `id_usulan` int(11) NOT NULL,
  `id_tim` int(11) NOT NULL,
  `nim_mhs` varchar(20) NOT NULL,
  `file_presentasi` text NOT NULL,
  `file_laporan_akhir` text NOT NULL,
  `file_poster` text NOT NULL,
  `file_dokumen_hki` text NOT NULL,
  `link_youtube_presentasi` text NOT NULL,
  `link_youtube_demo` text NOT NULL,
  `link_manual_book` text NOT NULL,
  `tgl_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_berkas_uts`
--

CREATE TABLE `tb_berkas_uts` (
  `id_berkas_uts` int(11) NOT NULL,
  `id_usulan` int(11) NOT NULL,
  `id_tim` int(11) NOT NULL,
  `nim_mhs` varchar(30) NOT NULL,
  `file_presentasi` text NOT NULL,
  `link_youtube` text NOT NULL,
  `link_tambahan` text NOT NULL,
  `tgl_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_daftar_matakuliah`
--

CREATE TABLE `tb_daftar_matakuliah` (
  `id_matakuliah` int(11) NOT NULL,
  `id_tahun_ajaran` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `kode_matakuliah` varchar(150) NOT NULL,
  `nama_matakuliah` varchar(150) NOT NULL,
  `dosen_koor` text NOT NULL,
  `capaian_matakuliah` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_event`
--

CREATE TABLE `tb_event` (
  `id_event` int(11) NOT NULL,
  `id_usulan` int(11) NOT NULL,
  `id_tim` int(11) NOT NULL,
  `event` varchar(150) NOT NULL,
  `mulai` datetime NOT NULL,
  `akhir` datetime NOT NULL,
  `deskripsi_agenda` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_event_mhs`
--

CREATE TABLE `tb_event_mhs` (
  `id_event_mhs` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `id_tim` int(11) NOT NULL,
  `nim_mhs` varchar(20) NOT NULL,
  `isi_komentar` text NOT NULL,
  `tgl_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_groupchat`
--

CREATE TABLE `tb_groupchat` (
  `id_groupchart` int(11) NOT NULL,
  `id_usulan` int(11) NOT NULL,
  `id_tim` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nim_mhs` varchar(20) NOT NULL,
  `isi_chat` text NOT NULL,
  `tgl_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kanban_board`
--

CREATE TABLE `tb_kanban_board` (
  `id_kanban_board` int(11) NOT NULL,
  `id_usulan` int(11) NOT NULL,
  `id_tim` int(11) NOT NULL,
  `nim_mhs` varchar(50) NOT NULL,
  `judul` varchar(150) NOT NULL,
  `isi_board` text NOT NULL,
  `state` enum('Backlog','To Do','In Progress','Done') NOT NULL,
  `due_date` date NOT NULL,
  `tgl_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_logbook`
--

CREATE TABLE `tb_logbook` (
  `id_logbook` int(11) NOT NULL,
  `id_usulan` int(11) NOT NULL,
  `id_tim` int(11) NOT NULL,
  `tahapan` varchar(50) NOT NULL,
  `detail_pengerjaan` text NOT NULL,
  `output` varchar(100) NOT NULL,
  `tgl_mulai` date NOT NULL,
  `tgl_selesai` date NOT NULL,
  `persentase` int(11) NOT NULL,
  `tgl_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tb_logbook`
--

INSERT INTO `tb_logbook` (`id_logbook`, `id_usulan`, `id_tim`, `tahapan`, `detail_pengerjaan`, `output`, `tgl_mulai`, `tgl_selesai`, `persentase`, `tgl_created`) VALUES
(22, 0, 0, 'analisis', 'dasd', 'lkskl', '2023-12-19', '2023-12-25', 121, 2147483647);

-- --------------------------------------------------------

--
-- Table structure for table `tb_mahasiswa`
--

CREATE TABLE `tb_mahasiswa` (
  `nim_mhs` varchar(20) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `email_polibatam` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `no_hp` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_makul_judul`
--

CREATE TABLE `tb_makul_judul` (
  `id_usulan` int(11) NOT NULL,
  `id_tim` int(11) NOT NULL,
  `id_matakuliah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_tim_pbl`
--

CREATE TABLE `tb_tim_pbl` (
  `id_tim` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_usulan` int(11) NOT NULL,
  `nama_tim` varchar(150) NOT NULL,
  `judul_pertim` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_tim_pbl_mhs`
--

CREATE TABLE `tb_tim_pbl_mhs` (
  `id_tim` int(11) NOT NULL,
  `nim_mhs` int(20) NOT NULL,
  `nama_mhs` varchar(150) NOT NULL,
  `peran` enum('ketua','anggota') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `instansi` varchar(150) NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `password` varchar(250) NOT NULL,
  `status` enum('unverified','eksternal','internal') NOT NULL,
  `role` enum('ketua','anggota') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `nama`, `email`, `instansi`, `jabatan`, `password`, `status`, `role`) VALUES
(1, 'fatimah', 'fatimah123@gmail.com', 'poltek', 'mahasiswa', '1234', 'eksternal', 'ketua'),
(22000, 'abdul ghafur', 'ghofurar18@gmail.com', 'poltek', 'mahasiswa', '123456', 'internal', 'anggota');

-- --------------------------------------------------------

--
-- Table structure for table `tb_usulan_judul`
--

CREATE TABLE `tb_usulan_judul` (
  `id_usulan` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_tahun_ajaran` int(11) NOT NULL,
  `jenis_usulan` varchar(50) NOT NULL,
  `tipe_proyek` varchar(50) NOT NULL,
  `judul_proyek` varchar(250) NOT NULL,
  `deskripsi_permasalahan` text NOT NULL,
  `prodi_asal` varchar(50) NOT NULL,
  `prodi_asal2` varchar(50) NOT NULL,
  `manpro` int(11) NOT NULL,
  `co-manpro` int(11) NOT NULL,
  `tgl_created` datetime NOT NULL DEFAULT current_timestamp(),
  `nama_narahubung` varchar(150) NOT NULL,
  `no_wa_narahubung` varchar(20) NOT NULL,
  `email_narahubung` varchar(150) NOT NULL,
  `jurusan` varchar(10) NOT NULL,
  `prodi_asal3` varchar(150) NOT NULL,
  `prodi_asal4` varchar(150) NOT NULL,
  `prodi_asal5` varchar(150) NOT NULL,
  `status` enum('Diajukan','Diterima','Ditolak','Dilanjutkan','Ditunda') NOT NULL,
  `alasan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_berkas_pamerin`
--
ALTER TABLE `tb_berkas_pamerin`
  ADD PRIMARY KEY (`id_berkas_pamerin`),
  ADD UNIQUE KEY `id_usulan` (`id_usulan`),
  ADD UNIQUE KEY `id_tim` (`id_tim`);

--
-- Indexes for table `tb_berkas_uas`
--
ALTER TABLE `tb_berkas_uas`
  ADD PRIMARY KEY (`id_berkas_uas`),
  ADD UNIQUE KEY `id_usulan` (`id_usulan`,`id_tim`,`nim_mhs`);

--
-- Indexes for table `tb_berkas_uts`
--
ALTER TABLE `tb_berkas_uts`
  ADD PRIMARY KEY (`id_berkas_uts`),
  ADD UNIQUE KEY `id_usulan` (`id_usulan`,`id_tim`,`nim_mhs`);

--
-- Indexes for table `tb_daftar_matakuliah`
--
ALTER TABLE `tb_daftar_matakuliah`
  ADD PRIMARY KEY (`id_matakuliah`),
  ADD UNIQUE KEY `id_user` (`id_user`);

--
-- Indexes for table `tb_event`
--
ALTER TABLE `tb_event`
  ADD PRIMARY KEY (`id_event`),
  ADD UNIQUE KEY `id_usulan` (`id_usulan`,`id_tim`);

--
-- Indexes for table `tb_event_mhs`
--
ALTER TABLE `tb_event_mhs`
  ADD PRIMARY KEY (`id_event_mhs`),
  ADD UNIQUE KEY `id_event` (`id_event`,`id_tim`,`nim_mhs`);

--
-- Indexes for table `tb_groupchat`
--
ALTER TABLE `tb_groupchat`
  ADD PRIMARY KEY (`id_groupchart`),
  ADD UNIQUE KEY `id_usulan` (`id_usulan`,`id_tim`,`id_user`,`nim_mhs`);

--
-- Indexes for table `tb_kanban_board`
--
ALTER TABLE `tb_kanban_board`
  ADD PRIMARY KEY (`id_kanban_board`),
  ADD UNIQUE KEY `id_usulan` (`id_usulan`,`id_tim`,`nim_mhs`);

--
-- Indexes for table `tb_logbook`
--
ALTER TABLE `tb_logbook`
  ADD PRIMARY KEY (`id_logbook`),
  ADD UNIQUE KEY `id_usulan` (`id_usulan`,`id_tim`);

--
-- Indexes for table `tb_mahasiswa`
--
ALTER TABLE `tb_mahasiswa`
  ADD PRIMARY KEY (`nim_mhs`);

--
-- Indexes for table `tb_makul_judul`
--
ALTER TABLE `tb_makul_judul`
  ADD PRIMARY KEY (`id_usulan`,`id_tim`,`id_matakuliah`),
  ADD UNIQUE KEY `id_usulan` (`id_usulan`,`id_tim`);

--
-- Indexes for table `tb_tim_pbl`
--
ALTER TABLE `tb_tim_pbl`
  ADD PRIMARY KEY (`id_tim`),
  ADD UNIQUE KEY `id_user` (`id_user`,`id_usulan`);

--
-- Indexes for table `tb_tim_pbl_mhs`
--
ALTER TABLE `tb_tim_pbl_mhs`
  ADD PRIMARY KEY (`id_tim`) USING BTREE,
  ADD UNIQUE KEY `nim_mhs` (`nim_mhs`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`) USING BTREE;

--
-- Indexes for table `tb_usulan_judul`
--
ALTER TABLE `tb_usulan_judul`
  ADD PRIMARY KEY (`id_usulan`),
  ADD UNIQUE KEY `id_user` (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_berkas_pamerin`
--
ALTER TABLE `tb_berkas_pamerin`
  MODIFY `id_berkas_pamerin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tb_berkas_uas`
--
ALTER TABLE `tb_berkas_uas`
  MODIFY `id_berkas_uas` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_berkas_uts`
--
ALTER TABLE `tb_berkas_uts`
  MODIFY `id_berkas_uts` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tb_daftar_matakuliah`
--
ALTER TABLE `tb_daftar_matakuliah`
  MODIFY `id_matakuliah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tb_event`
--
ALTER TABLE `tb_event`
  MODIFY `id_event` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_event_mhs`
--
ALTER TABLE `tb_event_mhs`
  MODIFY `id_event_mhs` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_groupchat`
--
ALTER TABLE `tb_groupchat`
  MODIFY `id_groupchart` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_kanban_board`
--
ALTER TABLE `tb_kanban_board`
  MODIFY `id_kanban_board` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tb_logbook`
--
ALTER TABLE `tb_logbook`
  MODIFY `id_logbook` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tb_tim_pbl`
--
ALTER TABLE `tb_tim_pbl`
  MODIFY `id_tim` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_tim_pbl_mhs`
--
ALTER TABLE `tb_tim_pbl_mhs`
  MODIFY `id_tim` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22001;

--
-- AUTO_INCREMENT for table `tb_usulan_judul`
--
ALTER TABLE `tb_usulan_judul`
  MODIFY `id_usulan` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
